module github.com/siongui/gopalilib

go 1.17

require (
	github.com/gopherjs/gopherjs v0.0.0-20210923143318-357ed63a84fb
	github.com/siongui/go-succinct-data-structure-trie v0.0.0-20210926213055-1e96df0b792f
	github.com/siongui/godom v0.0.0-20200920152407-31a6de96fdf6
	github.com/siongui/goef v0.0.0-20210610184109-d3b60554c5f3
	github.com/siongui/gojianfan v0.0.0-20210926212422-2f175ac615de
	github.com/winhtaikaung/Rabbit-Go v0.0.0-20190314045429-213403fa9212
	golang.org/x/net v0.0.0-20210924151903-3ad01bbaa167
)

require golang.org/x/text v0.3.6 // indirect
